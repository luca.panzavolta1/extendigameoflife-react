import { DEFAULTS } from "./types";

export const DEFAULT: DEFAULTS = {
  BOARD: [
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
  ],
  GENERATION: 0,
  SIZE: [7, 8],
};
