import { Board as IBoard } from "./types";
import { Cell } from "./Cell";

type BoardProps = {
  data: IBoard.DATA;
  onToggleCellStatus: (coords: IBoard.COORDINATES) => void;
};

export const Board = function ({ data, onToggleCellStatus }: BoardProps) {
  return (
    <div className="board">
      {data.map((row, rowIndex) => (
        <div key={`row_${rowIndex}`} className="row">
          {row.map((cellValue, colIndex) => (
            <Cell
              key={`col_${colIndex}`}
              isAlive={cellValue}
              coords={[rowIndex, colIndex]}
              onToggleCellStatus={onToggleCellStatus}
            />
          ))}
        </div>
      ))}
    </div>
  );
};
