export namespace Board {
  export type SIZE = [number, number];
  export type DATA = boolean[][];
  export type COORDINATES = [number, number];
  export type MATRIX = string[][];
  export type CELLSTATUS = boolean;
}

export enum CellStatus {
  ALIVE = "*",
  DEAD = ".",
}

export type DEFAULTS = {
  BOARD: Board.DATA;
  GENERATION: number;
  SIZE: Board.SIZE;
};
