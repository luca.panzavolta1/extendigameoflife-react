import { Board } from "./Board";
import { Board as IBoard } from "./types";
import { useGameOfLife } from "./useGameOfLife";

type GameProps = {
  initialBoard: boolean[][];
  initialGeneration: number;
  initialSize: IBoard.SIZE;
};

export const Game = function ({ initialBoard, initialGeneration, initialSize }: GameProps) {
  const [board, generation, toggleCellStatus, computeNextGeneration] = useGameOfLife(
    initialBoard,
    initialGeneration,
    initialSize
  );

  return (
    <>
      <h1>{`Generation n.${generation} board`}</h1>
      <Board data={board} onToggleCellStatus={toggleCellStatus} />
      <button className="nextGenButton" onClick={computeNextGeneration}>
        Next generation
      </button>
    </>
  );
};
