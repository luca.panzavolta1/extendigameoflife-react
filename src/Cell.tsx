import { Board as IBoard } from "./types";

type CellProps = {
  isAlive: IBoard.CELLSTATUS;
  coords: IBoard.COORDINATES;
  onToggleCellStatus: (cellCoords: IBoard.COORDINATES) => void;
};

export const Cell = function ({ isAlive, coords, onToggleCellStatus }: CellProps) {
  return (
    <div
      className="cell"
      style={{ backgroundColor: isAlive ? "yellow" : "gray" }}
      onClick={() => onToggleCellStatus(coords)}
    ></div>
  );
};
