import { Board as IBoard } from "./types";
import { useState, useEffect } from "react";

export function useGameOfLife(
  initialBoard: IBoard.DATA,
  initialGeneration: number,
  initialSize: IBoard.SIZE
): [IBoard.DATA, number, (c: IBoard.COORDINATES) => void, () => void] {
  const [board, setBoard] = useState<IBoard.DATA>(initialBoard);
  const [generation, setGeneration] = useState<number>(initialGeneration);
  const [boardSize, setBoardSize] = useState<IBoard.SIZE>(initialSize);

  useEffect(() => {
    setBoard(initialBoard);
    setGeneration(initialGeneration);
    setBoardSize(initialSize);
  }, [initialBoard, initialGeneration, initialSize]);

  const toggleCellStatus = function ([row, col]: IBoard.COORDINATES) {
    const currentCellStatus = board[row][col];
    const boardCopy = board.slice();
    boardCopy[row][col] = !currentCellStatus;
    setBoard(boardCopy);
  };

  const computeNextGeneration = () => {
    const nextGenerationBoard: IBoard.DATA = [];

    const [boardRows, boardColumns] = boardSize;
    for (let row = 0; row < boardRows; row++) {
      for (let col = 0; col < boardColumns; col++) {
        const cellStatus: IBoard.CELLSTATUS = board[row][col];
        const nextCellState = computeNextCellState(cellStatus, [row, col]);

        nextGenerationBoard[row]
          ? (nextGenerationBoard[row][col] = nextCellState)
          : (nextGenerationBoard[row] = [nextCellState]);
      }
    }

    setBoard(nextGenerationBoard);
    setGeneration((currentGeneration) => currentGeneration + 1);
  };

  const computeNextCellState = (cellStatus: IBoard.CELLSTATUS, cellCoords: IBoard.COORDINATES): IBoard.CELLSTATUS => {
    const aliveNeighboursCount = countAliveNeighbours(cellCoords);
    const cellIsAlive = cellStatus;

    let willCellBeAlive = cellStatus;

    if (cellIsAlive && aliveNeighboursCount < 2) willCellBeAlive = false;
    else if (cellIsAlive && aliveNeighboursCount > 3) willCellBeAlive = false;
    else if (!cellIsAlive && aliveNeighboursCount === 3) willCellBeAlive = true;

    return willCellBeAlive;
  };

  const countAliveNeighbours = (cellCoords: IBoard.COORDINATES): number => {
    const neighboursCoordinates: IBoard.COORDINATES[] = getNeighboursCoordinates(cellCoords);

    const neighboursAliveStatus = neighboursCoordinates
      .filter((neighbourCoordinates) => areCoordinatesValid(neighbourCoordinates))
      .map(([row, col]) => board[row][col]);

    const aliveNeighboursCount = neighboursAliveStatus.filter(Boolean).length;

    return aliveNeighboursCount;
  };

  const getNeighboursCoordinates = (cellCoords: IBoard.COORDINATES): IBoard.COORDINATES[] => {
    const [row, col] = cellCoords;

    const topLeft: IBoard.COORDINATES = [row - 1, col - 1];
    const top: IBoard.COORDINATES = [row - 1, col];
    const topRight: IBoard.COORDINATES = [row - 1, col + 1];
    const right: IBoard.COORDINATES = [row, col + 1];
    const bottomRight: IBoard.COORDINATES = [row + 1, col + 1];
    const bottom: IBoard.COORDINATES = [row + 1, col];
    const bottomLeft: IBoard.COORDINATES = [row + 1, col - 1];
    const left: IBoard.COORDINATES = [row, col - 1];

    return [topLeft, top, topRight, right, bottomRight, bottom, bottomLeft, left];
  };

  const areCoordinatesValid = (cellCoordinates: IBoard.COORDINATES): boolean => {
    const [x, y] = cellCoordinates;
    const [boardX, boardY] = boardSize;

    return x >= 0 && y >= 0 && x < boardX && y < boardY;
  };

  return [board, generation, toggleCellStatus, computeNextGeneration];
}
