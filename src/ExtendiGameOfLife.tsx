import { Game as GameOfLife } from "./GameOfLife";
import { useState, useEffect } from "react";
import { Board as IBoard, CellStatus } from "./types";
import { DEFAULT } from "./Defaults";

function ExtendiGameOfLife() {
  const [txtFile, setTxtFile] = useState<string>("");
  const [initialBoard, setInitialBoard] = useState<IBoard.DATA>(DEFAULT.BOARD);
  const [initialGeneration, setInitialGeneration] = useState<number>(DEFAULT.GENERATION);
  const [initialSize, setInitialSize] = useState<IBoard.SIZE>(DEFAULT.SIZE);

  useEffect(() => {
    if (txtFile) {
      initializeGame();
    }
  }, [txtFile]);

  // reads content of .txt file loaded by <input type="file">
  const readTxtGame = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e?.target?.files && e.target.files[0];
    if (file) {
      const reader = new FileReader();

      reader.onload = () => {
        setTxtFile(reader.result as string);
      };
      reader.readAsText(file);
    }
  };

  // extrapolates board data, generation and board size from .txt file content
  const initializeGame = () => {
    const fileLines = txtFile.split(/\n/);

    const generation = getGeneration(fileLines);
    const boardSize = getBoardSize(fileLines);
    const boardMatrix = getBoardMatrix(fileLines);
    const board = initializeBoard(boardMatrix);

    setInitialGeneration(generation);
    setInitialBoard(board);
    setInitialSize(boardSize);
  };

  const getGeneration = (fileLines: string[]): number => {
    // according to the .txt format provided
    // the generation number must be available on the file's first line
    const [firstLine] = fileLines;
    const regex = new RegExp(/\d+/, "g");
    const match = firstLine.match(regex);

    if (match) {
      const [generationAsString] = match;
      const generation = parseInt(generationAsString);
      return generation;
    }

    throw new Error("Generation not found.");
  };

  const getBoardSize = (fileLines: string[]): IBoard.COORDINATES => {
    // board size data must be available on the file's second line
    const [_, secondLine] = fileLines;
    const regex = /\d/g;
    const match = secondLine.match(regex);

    if (match) {
      const boardSizeAsStringArray = match as [string, string];
      const boardSize = boardSizeAsStringArray.map((x) => parseInt(x)) as [number, number];
      return boardSize;
    }

    throw new Error("Board size not found.");
  };

  const getBoardMatrix = (fileLines: string[]): IBoard.MATRIX => {
    // board matrix data must be available from the file's third line onwards
    const [_, __, ...boardRawData] = fileLines;
    const boardMatrix = boardRawData.map((row) => row.split(/ /));
    return boardMatrix;
  };

  // creates an array of arrays representing the board
  // it also replaces the '*' character with 'true' and '.' character with 'false'
  // 'true' represent a live cell while 'false' represents a dead cell
  const initializeBoard = (boardMatrix: IBoard.MATRIX): IBoard.DATA => {
    const board: IBoard.DATA = [];

    for (let row = 0; row < boardMatrix.length; row++) {
      for (let col = 0; col < boardMatrix[row].length; col++) {
        const cell = boardMatrix[row][col] === CellStatus.ALIVE ? true : false;
        board[row] ? board[row].push(cell) : (board[row] = [cell]);
      }
    }

    return board;
  };

  return (
    <div className="App" style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
      <input type="file" accept="text/plain" onChange={readTxtGame} />

      <GameOfLife initialBoard={initialBoard} initialGeneration={initialGeneration} initialSize={initialSize} />
    </div>
  );
}

export default ExtendiGameOfLife;
