# Extendi's Game Of Life - Luca Panzavolta's Coding challenge

![GameOfLife Board Representation](./game-of-life-react-representation.png)

## How to run the project

Run the following commands to correctly setup the project.

1. `git clone https://gitlab.com/luca.panzavolta1/extendigameoflife-react.git`
2. `cd extendigameoflife-react`
3. `npm i`
4. `npm start`

Running the commands above will open up a browser window at [http://localhost:3000 ](http://localhost:3000)

## How to play the game

You can play in two ways:

1. Load up the .txt file containing the game's data (use `test.txt` or `test2.txt` provided)
2. Play freely interacting with the board

## Next steps

Things that could be added to the codebase to improve it:

1. stricter file validation and parsing checks
2. add Unit Testing
